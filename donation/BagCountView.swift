//
//  RegisterController.swift
//  donation
//
//  Created by 이영인 on 2016. 7. 12..
//  Copyright © 2016년 이영인. All rights reserved.
//

import UIKit

class BagCountView: UIViewController {

    @IBOutlet var bagCount: UILabel!

    @IBAction func bagAdd(_ sender: AnyObject) {
        if let count = Int(bagCount.text!) {
            bagCount.text = "\(count + 1)"
        }
    }

    @IBAction func bagSub(_ sender: AnyObject) {
        if let count = Int(bagCount.text!) {
            if count > 1 {
                bagCount.text = "\(count - 1)"
            }
        }
    }

    @IBAction func next(_ sender: AnyObject) {
        let prefs:UserDefaults = UserDefaults.standard
        prefs.set(bagCount.text, forKey: "BAGCOUNT")
        prefs.synchronize()
        self.performSegue(withIdentifier: "set_bagcount", sender: self)
    }

    override func viewDidLoad() {
        super.viewDidLoad()
    }
}
