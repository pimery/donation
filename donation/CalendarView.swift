//
//  CalendarView.swift
//  donation
//
//  Created by 이영인 on 2016. 8. 26..
//  Copyright © 2016년 이영인. All rights reserved.
//

import UIKit

class CalendarView: UIViewController {
    
    var index: Int = 0
    var isSelected: Bool = false
    var date: Date!
    let dateForm = DateFormat()
    
    @IBOutlet var backImage: UIImageView!
    
    @IBAction func next(_ sender: AnyObject) {
        if (index != 0) {
            if date == nil {
                date = dateForm.defaultDate() as Date!
            } else {
                date = dateForm.change(date, num: index)
            }
            isSelected = true
        } else {
            isSelected = false
        }
        self.performSegue(withIdentifier: "select_date", sender: self)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        backImage.image = UIImage(named: "1_9")
        for i in 0 ..< 7 {
            if (i == 5) {
                continue
            }
            let x = 130 + (42 * i)
            var frame = CGRect(x: x, y: 362, width: 30, height: 30)
            if (i == 6) {
                frame.origin.x = 88
                frame.origin.y = 400
            }
            let btn = UIButton(frame: frame)
            btn.setTitleColor(UIColor.clear, for: UIControlState())
            btn.setTitle(String(i + 1), for: UIControlState())
            btn.addTarget(self, action: #selector(CalendarView.btnAct(_:)), for: .touchUpInside)
            self.view.addSubview(btn)
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let ok = segue.destination as? OKview {
            if isSelected {
                ok.data = ["1_11", "Donate", "DateSelectView"]
                ok.date = date
            } else {
                ok.data = ["1_10", "Donate", "CalendarView"]
            }
        }
    }
    
    func btnAct(_ sender: UIButton!) {
        index = Int(sender.currentTitle!)!
        backImage.image = UIImage(named: "1_9-\(index)")
    }
}
