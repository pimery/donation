//
//  CheckView.swift
//  donation
//
//  Created by 이영인 on 2016. 7. 26..
//  Copyright © 2016년 이영인. All rights reserved.
//

import UIKit

class CheckView: UIViewController {

    let prefs:UserDefaults = UserDefaults.standard
    var isChecked:Bool = false
    
    @IBOutlet var dateLabel: UILabel!
    @IBOutlet var backImage: UIImageView!

    @IBAction func checkBtn(_ sender: UIButton) {
        if isChecked {
            backImage.image = UIImage(named: "1_18")
            isChecked = false
        } else {
            backImage.image = UIImage(named: "1_19")
            isChecked = true
        }
    }

    @IBAction func next(_ sender: AnyObject) {
        if isChecked {
            prefs.set(1, forKey: "ISDONATED")
            prefs.removeObject(forKey: "ISEXTENDED")
            prefs.synchronize()
        }
        self.performSegue(withIdentifier: "check_guide", sender: self)
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        let date:String = prefs.string(forKey: "DATE") as String!
        dateLabel.text = date
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let ok = segue.destination as? OKview {
            if isChecked {
                ok.data = ["1_21", "Home", "HomeView"]
            } else {
                ok.data = ["1_20", "Donate", "CheckView"]
            }
        }
    }
}
