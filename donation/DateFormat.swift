//
//  DateFormat.swift
//  donation
//
//  Created by 이영인 on 2016. 8. 23..
//  Copyright © 2016년 이영인. All rights reserved.
//

import Foundation

class DateFormat {
    
    let WeekDay = ["", "일", "월", "화", "수", "목", "금", "토"]
    let format: DateFormatter = DateFormatter()
    
    init() {
        format.locale = Locale(identifier: "ko_kr")
        format.timeZone = TimeZone(identifier: "KST")
        format.dateFormat = "yyyy.M.d."
    }
    
    func form(_ date: Date) -> String {
        let text: String = format.string(from: date);
        let calendar = Calendar(identifier: Calendar.Identifier.gregorian)
        let comp = (calendar as NSCalendar).components(.weekday, from: format.date(from: text)!)
        return text + "\(WeekDay[comp.weekday!])"
    }
    
    func change(_ date: Date, num: Int) -> Date {
        let changed = (Calendar.current as NSCalendar)
            .date(
                byAdding: .day,
                value: num,
                to: date,
                options: []
            )!
        return changed
    }
    
    func change(_ text: String, num: Int) -> String {
        let index = text.characters.index(text.endIndex, offsetBy: -1)
        let truncated = text.substring(to: index)
        let date = format.date(from: truncated)!
        let changed = (Calendar.current as NSCalendar)
            .date(
                byAdding: .day,
                value: num,
                to: date,
                options: []
        )!
        return self.form(changed)
    }
    
    func defaultDate() -> Date {
        let date: Date = format.date(from: "2016.10.24")!
        return date
    }
}
