//
//  DateSelectView.swift
//  donation
//
//  Created by 이영인 on 2016. 7. 26..
//  Copyright © 2016년 이영인. All rights reserved.
//

import UIKit

class DateSelectView: UIViewController {

    var date: Date!
    let form = DateFormat()
    
    @IBOutlet var dateLabel: UILabel!

    @IBAction func next(_ sender: AnyObject) {
        let prefs:UserDefaults = UserDefaults.standard
        prefs.set(dateLabel.text, forKey: "DATE")
        prefs.synchronize()
        self.performSegue(withIdentifier: "set_date", sender: self)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if date == nil {
            date = form.defaultDate() as Date!
        }
        dateLabel.text = form.form(date)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let calendar = segue.destination as? CalendarView {
            calendar.date = date
        }
    }
}
