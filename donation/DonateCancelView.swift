//
//  DonateCancel.swift
//  donation
//
//  Created by 이영인 on 2016. 8. 26..
//  Copyright © 2016년 이영인. All rights reserved.
//

import UIKit

class DonateCancelView: UIViewController {
    
    @IBOutlet var backImage: UIImageView!
    
    @IBAction func yes_down(_ sender: AnyObject) {
        backImage.image = UIImage(named: "1_6y")
    }
    @IBAction func no_down(_ sender: AnyObject) {
        backImage.image = UIImage(named: "1_6n")
    }
    
    @IBAction func no(_ sender: AnyObject) {
        self.dismiss(animated: false, completion: nil)
    }
}
