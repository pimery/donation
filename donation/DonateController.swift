//
//  DonateController.swift
//  donation
//
//  Created by 이영인 on 2016. 8. 25..
//  Copyright © 2016년 이영인. All rights reserved.
//

import UIKit

class DonateController: UIViewController {
    
    var isDonated:Int = 0
    var prefs:UserDefaults = UserDefaults.standard
    
    @IBOutlet var backImage: UIImageView!
    
    @IBAction func next(_ sender: AnyObject) {
        if (isDonated != 1) {
            self.performSegue(withIdentifier: "go_donate", sender: self)
        } else {
            prefs.removeObject(forKey: "ISDONATED")
            self.performSegue(withIdentifier: "cancel_donate", sender: self)
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidLoad()
        isDonated = prefs.integer(forKey: "ISDONATED") as Int
        if (isDonated != 1) {
            backImage.image = UIImage(named: "1_0")
        } else {
            let proj = prefs.integer(forKey: "BAGPROJ") as Int
            backImage.image = UIImage(named: "1_22-\(proj)")
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let ok = segue.destination as? OKview {
            ok.data = ["1_23", "Home", "HomeView"]
        }
    }
}
