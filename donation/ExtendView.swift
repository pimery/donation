//
//  ExtendView.swift
//  donation
//
//  Created by 이영인 on 2016. 8. 23..
//  Copyright © 2016년 이영인. All rights reserved.
//

import UIKit

class ExtendView: UIViewController {
    
    var prefs:UserDefaults = UserDefaults.standard
    let dateForm = DateFormat()
    
    @IBOutlet var backImage: UIImageView!
    @IBOutlet var doneBtn: UIButton!
    @IBOutlet var btn3: UIButton!
    @IBOutlet var btn4: UIButton!
    @IBOutlet var btn5: UIButton!
    @IBOutlet var closeBtn: UIButton!
    
    @IBAction func Down3(_ sender: AnyObject) {
        backImage.image = UIImage(named: "2_2-1")
    }
    
    @IBAction func Down4(_ sender: AnyObject) {
        backImage.image = UIImage(named: "2_2-2")
    }
    
    @IBAction func Down5(_ sender: AnyObject) {
        backImage.image = UIImage(named: "2_2-3")
    }
    
    @IBAction func Extend3(_ sender: AnyObject) {
        extend(3)
    }
    @IBAction func Extend4(_ sender: AnyObject) {
        extend(4)
    }
    @IBAction func Extend5(_ sender: AnyObject) {
        extend(5)
    }
    
    @IBAction func done_down(_ sender: AnyObject) {
        backImage.image = UIImage(named: "2_3y")
    }
    
    @IBAction func done(_ sender: AnyObject) {
        let homeController = self.storyboard?.instantiateViewController(withIdentifier: "HomeView") as! UITabBarController
        homeController.selectedIndex = 1
        self.present(homeController, animated: false, completion: nil)
    }
    
    override func viewDidLoad() {
        doneBtn.alpha = 0
    }
    
    func extend(_ num: Int) {
        let date:String = prefs.string(forKey: "DATE") as String!
        prefs.set(dateForm.change(date, num: num), forKey: "DATE")
        prefs.set(1, forKey: "ISEXTENDED")
        prefs.synchronize()
        
        backImage.image = UIImage(named: "2_3")
        doneBtn.alpha = 1
        btn3.alpha = 0
        btn4.alpha = 0
        btn5.alpha = 0
        closeBtn.alpha = 0
    }
}
