//
//  LoginController.swift
//  donation
//
//  Created by 이영인 on 2016. 7. 12..
//  Copyright © 2016년 이영인. All rights reserved.
//

import UIKit

class LoginController: UIViewController {

    @IBOutlet var usernameText: UITextField!
    @IBOutlet var passwordText: UITextField!
    
    let prefs:UserDefaults = UserDefaults.standard

    @IBAction func loginTapped(_ sender: AnyObject) {
        if let username: NSString = usernameText.text as NSString?,
        let password: NSString = passwordText.text as NSString? {
            if (username.isEqual(to: "cmyk") || password.isEqual(to: "1210")) {
                prefs.set(1, forKey: "ISLOGGED")
                prefs.synchronize()
                self.performSegue(withIdentifier: "goto_home", sender: self)
            } else {
                let alert = UIAlertController(title: "로그인 실패", message: "다시 시도해주세요", preferredStyle: .alert)
                let OKAction = UIAlertAction(title: "OK", style: .default) { (action:UIAlertAction!) in }
                alert.addAction(OKAction)
                self.present(alert, animated: true, completion:nil)
            }
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        NotificationCenter.default.addObserver(self, selector: #selector(LoginController.keyboardWillShow(_:)), name:NSNotification.Name.UIKeyboardWillShow, object: self.view.window)
        NotificationCenter.default.addObserver(self, selector: #selector(LoginController.keyboardWillHide(_:)), name:NSNotification.Name.UIKeyboardWillHide, object: self.view.window)
        prefs.removeObject(forKey: "ISLOGGED")
        prefs.synchronize()
        self.hideKeyboardWhenTappedAround()
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIKeyboardWillShow, object: self.view.window)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIKeyboardWillHide, object: self.view.window)
    }
    
    var movedUp = false
    
    func keyboardWillShow(_ sender: Notification) {
        if (movedUp) {
            return
        }
        let userInfo: [AnyHashable: Any] = (sender as NSNotification).userInfo!
        let keyboardSize: CGSize = (userInfo[UIKeyboardFrameBeginUserInfoKey]! as AnyObject).cgRectValue.size
        let offset: CGSize = (userInfo[UIKeyboardFrameEndUserInfoKey]! as AnyObject).cgRectValue.size
        if keyboardSize.height == offset.height {
            UIView.animate(withDuration: 0.1, animations: { () -> Void in
                self.view.frame.origin.y -= keyboardSize.height
            })
        } else {
            UIView.animate(withDuration: 0.1, animations: { () -> Void in
                self.view.frame.origin.y += keyboardSize.height - offset.height
            })
        }
        movedUp = true
    }
    
    func keyboardWillHide(_ sender: Notification) {
        if (movedUp == false) {
            return
        }
        let userInfo: [AnyHashable: Any] = (sender as NSNotification).userInfo!
        let keyboardSize: CGSize = (userInfo[UIKeyboardFrameBeginUserInfoKey]! as AnyObject).cgRectValue.size
        self.view.frame.origin.y += keyboardSize.height
        movedUp = false
    }
}

extension UIViewController {
    func hideKeyboardWhenTappedAround() {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(UIViewController.dismissKeyboard))
        view.addGestureRecognizer(tap)
    }

    func dismissKeyboard() {
        view.endEditing(true)
    }
}
