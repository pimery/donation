//
//  Menu.swift
//  donation
//
//  Created by 이영인 on 2016. 8. 26..
//  Copyright © 2016년 이영인. All rights reserved.
//

import UIKit

class Menu: UIViewController {
    
    @IBOutlet var backImage: UIImageView!
    
    @IBAction func close(_ sender: AnyObject) {
        self.dismiss(animated: false, completion: nil)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let prefs:UserDefaults = UserDefaults.standard
        let isDonated = prefs.integer(forKey: "ISDONATED") as Int
        if (isDonated == 1) {
            backImage.image = UIImage(named: "menu_1")
        }
    }
}
