//
//  MypageController.swift
//  donation
//
//  Created by 이영인 on 2016. 8. 26..
//  Copyright © 2016년 이영인. All rights reserved.
//

import UIKit

class MypageController: UITabBarController, UITabBarControllerDelegate {

    override func viewDidLoad() {
        super.viewDidLoad()
        self.delegate = self
    }
    
    override func viewWillLayoutSubviews() {
        var tabFrame = self.tabBar.frame
        tabFrame.size.height = 49
        tabFrame.origin.y = 52
        self.tabBar.frame = tabFrame
        self.tabBar.backgroundImage = UIImage()
        self.tabBar.shadowImage = UIImage()
    }
}