//
//  OKview.swift
//  donation
//
//  Created by 이영인 on 2016. 9. 6..
//  Copyright © 2016년 이영인. All rights reserved.
//

import UIKit

class OKview: UIViewController {
    
    var data:[String] = []
    let IMG = 0
    let BRD = 1
    let VIW = 2
    var date: Date!
    
    @IBOutlet var backImage: UIImageView!
    
    @IBAction func down_y(_ sender: AnyObject) {
        backImage.image = UIImage(named: "\(data[IMG])y")
    }
    
    @IBAction func done(_ sender: AnyObject) {
        data[IMG] = ""
        let storyboard = UIStoryboard(name: data[BRD], bundle: nil)
        if date != nil {
            let vc = storyboard.instantiateViewController(withIdentifier: data[VIW]) as! DateSelectView
            vc.date = date
            self.present(vc, animated: false, completion: nil)
        } else {
            let vc = storyboard.instantiateViewController(withIdentifier: data[VIW])
            self.present(vc, animated: false, completion: nil)
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if data[IMG] != "" {
            backImage.image = UIImage(named: data[IMG])
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let vw = segue.destination as? DateSelectView {
            vw.date = date
        }
    }
}
