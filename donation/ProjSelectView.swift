//
//  ProjSelectView.swift
//  donation
//
//  Created by 이영인 on 2016. 9. 11..
//  Copyright © 2016년 이영인. All rights reserved.
//

import UIKit

class ProjSelectView: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    var cells: [ProjCell] = [ProjCell]()
    var selected: Int = 0
    @IBOutlet var table: UITableView!
    
    @IBAction func done(_ sender: AnyObject) {
        if (selected < 1) {
            self.performSegue(withIdentifier: "select_proj", sender: self)
        } else {
            let prefs:UserDefaults = UserDefaults.standard
            prefs.set(selected, forKey: "BAGPROJ")
            prefs.synchronize()
            self.performSegue(withIdentifier: "selected_proj", sender: self)
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        table.delegate = self
        table.dataSource = self
        
        for i in 0 ..< 4 {
            let cell:ProjCell = table.dequeueReusableCell(withIdentifier: "cell", for: IndexPath(row: i, section: 0)) as! ProjCell
            cell.img.image = UIImage(named: "1_3_\(i + 1)")
            cell.backgroundView = nil
            cell.backgroundColor = UIColor.clear
            cells.append(cell)
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let ok = segue.destination as? OKview {
            ok.data = ["1_5", "Donate", "ProjSelectView"]
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 4
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell:ProjCell = cells[(indexPath as NSIndexPath).row]
        if (cell.sel) {
            cell.img.image = UIImage(named: "1_3_\((indexPath as NSIndexPath).row + 1)y")
        } else {
            cell.img.image = UIImage(named: "1_3_\((indexPath as NSIndexPath).row + 1)")
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        for i in 0 ..< 4 {
            if (i == (indexPath as NSIndexPath).row) {
                cells[i].sel = true
                cells[i].img.image = UIImage(named: "1_3_\(i + 1)y")
                selected = i + 1
            } else {
                cells[i].sel = false
                cells[i].img.image = UIImage(named: "1_3_\(i + 1)")
            }
        }
    }
}
