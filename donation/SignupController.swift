//
//  SignupController.swift
//  donation
//
//  Created by 이영인 on 2016. 7. 12..
//  Copyright © 2016년 이영인. All rights reserved.
//

import UIKit

class SignController: UIViewController {

    @IBOutlet var textUsername: UITextField!
    @IBOutlet var textPass1: UITextField!
    @IBOutlet var textPass2: UITextField!
    @IBOutlet var textAddr: UITextField!

    @IBAction func signupTapped(_ sender: UIButton) {
        if let username: NSString = textUsername.text as NSString?,
        let pass1: NSString = textPass1.text as NSString?,
        let pass2: NSString = textPass2.text as NSString?,
        let addr: NSString = textAddr.text as NSString? {
            let alert = UIAlertController(title: "회원가입 실패", message: "모두 입력해주세요", preferredStyle: .alert)
            let OKAction = UIAlertAction(title: "확인", style: .default) { (action:UIAlertAction!) in }
            alert.addAction(OKAction)
            
            if (username.isEqual(to: "") || pass1.isEqual(to: "") || pass2.isEqual(to: "") || addr.isEqual(to: "")) {
                self.present(alert, animated: true, completion:nil)
            } else if (!pass1.isEqual(pass2)) {
                alert.message = "비밀번호를 확인해주세요"
                self.present(alert, animated: true, completion:nil)
            } else {
                self.performSegue(withIdentifier: "goto_login", sender: self)
            }
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        NotificationCenter.default.addObserver(self, selector: #selector(LoginController.keyboardWillShow(_:)), name:NSNotification.Name.UIKeyboardWillShow, object: self.view.window)
        NotificationCenter.default.addObserver(self, selector: #selector(LoginController.keyboardWillHide(_:)), name:NSNotification.Name.UIKeyboardWillHide, object: self.view.window)
        self.hideKeyboardWhenTappedAround()
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIKeyboardWillShow, object: self.view.window)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIKeyboardWillHide, object: self.view.window)
    }
    
    var movedUp = false
    
    func keyboardWillShow(_ sender: Notification) {
        if (movedUp) {
            return
        }
        let userInfo: [AnyHashable: Any] = (sender as NSNotification).userInfo!
        let keyboardSize: CGSize = (userInfo[UIKeyboardFrameBeginUserInfoKey]! as AnyObject).cgRectValue.size
        let offset: CGSize = (userInfo[UIKeyboardFrameEndUserInfoKey]! as AnyObject).cgRectValue.size
        if keyboardSize.height == offset.height {
            UIView.animate(withDuration: 0.1, animations: { () -> Void in
                self.view.frame.origin.y -= keyboardSize.height
            })
        } else {
            UIView.animate(withDuration: 0.1, animations: { () -> Void in
                self.view.frame.origin.y += keyboardSize.height - offset.height
            })
        }
        movedUp = true
    }
    
    func keyboardWillHide(_ sender: Notification) {
        if (movedUp == false) {
            return
        }
        let userInfo: [AnyHashable: Any] = (sender as NSNotification).userInfo!
        let keyboardSize: CGSize = (userInfo[UIKeyboardFrameBeginUserInfoKey]! as AnyObject).cgRectValue.size
        self.view.frame.origin.y += keyboardSize.height
        movedUp = false
    }
}
