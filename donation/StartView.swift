//
//  StartView.swift
//  donation
//
//  Created by 이영인 on 2016. 8. 30..
//  Copyright © 2016년 이영인. All rights reserved.
//

import UIKit

class StartView: UIViewController {
    @IBAction func start(_ sender: AnyObject) {
        let prefs:UserDefaults = UserDefaults.standard
        let islogged: Int = prefs.integer(forKey: "ISLOGGED") as Int
        if (islogged == 1) {
            self.performSegue(withIdentifier: "jumpto_home", sender: self)
        } else {
            self.performSegue(withIdentifier: "go_login", sender: self)
        }
    }
}
