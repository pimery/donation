//
//  TabDate.swift
//  donation
//
//  Created by 이영인 on 2016. 7. 24..
//  Copyright © 2016년 이영인. All rights reserved.
//

import UIKit

class TabDate: UIViewController {
    
    var isDonated:Int = 0
    var isExtended:Int = 0
    
    @IBOutlet var dateLabel: UILabel!
    @IBOutlet var backImage: UIImageView!
    
    @IBAction func next(_ sender: AnyObject) {
        if (isDonated == 1 && isExtended != 1) {
            self.performSegue(withIdentifier: "goto_extend", sender: self)
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        let prefs:UserDefaults = UserDefaults.standard
        isDonated = prefs.integer(forKey: "ISDONATED") as Int
        if (isDonated != 1) {
            backImage.image = UIImage(named: "2_0")
            dateLabel.alpha = 0
        } else {
            dateLabel.alpha = 1
            isExtended = prefs.integer(forKey: "ISEXTENDED") as Int
            if (isExtended != 1) {
                backImage.image = UIImage(named: "2_1")
            } else {
                backImage.image = UIImage(named: "2_5")
            }
            let date:String = prefs.string(forKey: "DATE") as String!
            dateLabel.text = date
        }
    }
}
