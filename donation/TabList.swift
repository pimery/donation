//
//  TabList.swift
//  donation
//
//  Created by 이영인 on 2016. 8. 26..
//  Copyright © 2016년 이영인. All rights reserved.
//

import UIKit

class TabList: UIViewController {
    
    let prefs:UserDefaults = UserDefaults.standard
    var index:Int = 1
    let popBtn = UIButton(frame: CGRect(x:0, y: 431, width: 375, height: 122))
    var btns: [UIButton] = [UIButton]()
    var alignNum: Int = 6
    var ifDonated: String = "_0"
    
    @IBOutlet var backImage: UIImageView!
    
    @IBAction func donate(_ sender: AnyObject) {
        if (ifDonated.characters.count > 0) {
            let storyboard = UIStoryboard(name: "Donate", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "BagCountView") as! BagCountView
            self.present(vc, animated: false, completion: nil)
        }
    }
    
    @IBAction func align_date(_ sender: AnyObject) {
        if (popBtn.alpha == 0) {
            alignNum = 6
            backImage.image = UIImage(named: back(6))
        }
    }
    @IBAction func align_weight(_ sender: AnyObject) {
        if (popBtn.alpha == 0) {
            alignNum = 7
            backImage.image = UIImage(named: back(7))
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidLoad()
        
        let isDonated = prefs.integer(forKey: "ISDONATED") as Int
        if (isDonated == 1) {
            ifDonated = ""
        } else {
            ifDonated = "_0"
        }
        
        backImage.image = UIImage(named: back(6))
        for i in 0 ..< 7 {
            let y = 135 + (44 * i)
            let btn = UIButton(frame: CGRect(x: 15, y: y, width: 345, height: 32))
            btn.setTitleColor(UIColor.clear, for: UIControlState())
            btn.setTitle(String(i + 1), for: UIControlState())
            btn.addTarget(self, action: #selector(TabList.btnAct(_:)), for: .touchUpInside)
            self.view.addSubview(btn)
            btns.append(btn)
        }
        popBtn.addTarget(self, action: #selector(TabList.out(_:)), for: .touchUpInside)
        popBtn.alpha = 0
        self.view.addSubview(popBtn)
    }
    
    func btnAct(_ sender: UIButton!) {
        index = Int(sender.currentTitle!)!
        backImage.image = UIImage(named: "3_\(alignNum)-\(index)")
        popBtn.alpha = 1
        for i in 0 ..< 7 {
            btns[i].alpha = 0
        }
    }
    
    func out(_ sender: UIButton!) {
        backImage.image = UIImage(named: back(alignNum))
        popBtn.alpha = 0
        for i in 0 ..< 7 {
            btns[i].alpha = 1
        }
    }
    
    func back(_ num: Int) -> String {
        return "3_\(num)\(ifDonated)"
    }
}
