//
//  TabMypage.swift
//  donation
//
//  Created by 이영인 on 2016. 7. 24..
//  Copyright © 2016년 이영인. All rights reserved.
//

import UIKit

class TabMypage: UIViewController {
    
    let prefs:UserDefaults = UserDefaults.standard
    var isDonated: Int = 0
    var index = 1
    let animationDuration: TimeInterval = 0.25
    let switchingInterval: TimeInterval = 2
    @IBOutlet var backImage: UIImageView!
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidLoad()
        
        isDonated = prefs.integer(forKey: "ISDONATED") as Int
        if (isDonated != 1) {
            backImage.image = UIImage(named: "3_0")
            index = 1
        } else {
            animateImageView()
        }
    }
    
    func animateImageView() {
        if (index > 5) {
            prefs.removeObject(forKey: "ISDONATED")
            prefs.synchronize()
            return
        }
        CATransaction.begin()
        
        CATransaction.setAnimationDuration(animationDuration)
        CATransaction.setCompletionBlock {
            let delay = DispatchTime.now() + Double(Int64(self.switchingInterval * TimeInterval(NSEC_PER_SEC))) / Double(NSEC_PER_SEC)
            DispatchQueue.main.asyncAfter(deadline: delay) {
                self.animateImageView()
            }
        }
        
        let transition = CATransition()
        transition.type = kCATransitionFade
        backImage.layer.add(transition, forKey: kCATransition)
        backImage.image = UIImage(named: "3_\(index)")
        index += 1
        CATransaction.commit()
    }
}
