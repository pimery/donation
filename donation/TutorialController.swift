//
//  TutorialController.swift
//  donation
//
//  Created by 이영인 on 2016. 7. 11..
//  Copyright © 2016년 이영인. All rights reserved.
//

import UIKit

class TutorialController: UIPageViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        dataSource = self
        if let firstViewController = orderedViewControllers.first {
            setViewControllers([firstViewController],
                    direction: .forward,
                    animated: true,
                    completion: nil)
        }
    }

    fileprivate(set) lazy var orderedViewControllers: [UIViewController] = {
        return [self.newColoredViewController(1),
                self.newColoredViewController(2),
                self.newColoredViewController(3),
                self.newColoredViewController(4)]
    }()

    fileprivate func newColoredViewController(_ num: Int) -> UIViewController {
        return UIStoryboard(name: "Main", bundle: nil) .
            instantiateViewController(withIdentifier: "Tutorial\(num)")
    }
}

extension TutorialController: UIPageViewControllerDataSource {
    
    func pageViewController(_ pageViewController: UIPageViewController,
                            viewControllerBefore viewController: UIViewController) -> UIViewController? {
        guard let viewControllerIndex = orderedViewControllers.index(of: viewController) else {
            return nil
        }
        let previousIndex = viewControllerIndex - 1
        guard previousIndex >= 0 else {
            return nil
        }
        guard orderedViewControllers.count > previousIndex else {
            return nil
        }
        return orderedViewControllers[previousIndex]
    }

    func pageViewController(_ pageViewController: UIPageViewController,
                            viewControllerAfter viewController: UIViewController) -> UIViewController? {
        guard let viewControllerIndex = orderedViewControllers.index(of: viewController) else {
            return nil
        }
        let nextIndex = viewControllerIndex + 1
        let orderedViewControllersCount = orderedViewControllers.count
        guard orderedViewControllersCount != nextIndex else {
            return nil
        }
        guard orderedViewControllersCount > nextIndex else {
            return nil
        }
        return orderedViewControllers[nextIndex]
    }
}
